﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kolokvij  {
    class GlasovniPoziv :Poziv , IPoziv {

        public GlasovniPoziv(DateTime vrijemePocetka, DateTime vrijemeZavrsetka, string lozinka) : base(vrijemePocetka, vrijemeZavrsetka, "123456") { }

        public override void SpojiKorisnika(Korisnik korisnik) {
            if (korisnik.Mic == true) {
                Console.WriteLine("Korisnik je spojen u glasovni poziv");
                korisnici.Add(korisnik);
            } else {
                throw new Exception("Korisnik nema mikrofon");
            }
        }

        public void PrikaziKorisnike(Korisnik korisnik) {
            if(korisnik.Cam == false) {
                Console.WriteLine(korisnici);
            }
        }

    }
}
                


