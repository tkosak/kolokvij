﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kolokvij {
    public sealed class Korisnik {

        public Korisnik(string korIme, string ime, int jedBroj=100000, bool mic = true, bool cam = true) {

            KorIme = korIme;
            Ime = ime;
            JedBroj = jedBroj++;
            Mic = mic;
            Cam = cam;
        }

        public string KorIme { get; }
        public string Ime { get; }
        public int JedBroj { get { return JedBroj; }
            set { if (value <= 100000) { throw new Exception("Broj je manji od 100000!"); } else { JedBroj = value; } } }
        public bool Mic { get; }
        public bool Cam { get; }

        public override string ToString() {
            return string.Format("Broj korisnika: {0} / korisnicko ime: {1} / ime: {2} ", JedBroj, KorIme, Ime);
        }
    }
}
