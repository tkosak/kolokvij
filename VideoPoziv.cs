﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kolokvij {
    class VideoPoziv : Poziv , IPoziv {

    
        public VideoPoziv(DateTime vrijemePocetka, DateTime vrijemeZavrsetka, string lozinka) : base(vrijemePocetka, vrijemeZavrsetka, "123456") { }

        public override void SpojiKorisnika(Korisnik korisnik) {
            if (korisnik.Mic == true && korisnik.Cam == true) {
                Console.WriteLine("Korisnik je spojen u video poziv");
                korisnici.Add(korisnik);
            } else {
                throw new Exception("Korisnik nema mikrofon/kameru");
            }
        }

    }
}


