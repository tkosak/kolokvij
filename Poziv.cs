﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kolokvij {
    public abstract class Poziv {

        protected Poziv(DateTime timePocetak, DateTime timeZavrsetak, string lozinka) {
            TimePocetak = timePocetak;
            TimeZavrsetak = timeZavrsetak;
            Lozinka = lozinka;
        }

       public string Korisnik;
        protected List<Korisnik> korisnici = new List<Korisnik>();

        DateTime TimePocetak { get { return TimePocetak; } set { } }
        DateTime TimeZavrsetak {
            get { return TimeZavrsetak; }
            set {
                if (value <= TimePocetak) {
                    throw new Exception("Nije moguce postaviti novo vrijeme zavrsetka!");
                } else {
                    value = TimeZavrsetak;
                }
            }
        }
        string Lozinka { get; set; }




        public abstract void SpojiKorisnika(Korisnik korisnik);

        public void PrikaziKorisnike() {
            foreach (var korisnik in korisnici) {
                Console.WriteLine(korisnik.ToString());
            }
        }
    }
}
